﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arrays1
{
    class Program
    {
        static void Main(string[] args)
        {
            double b, c, max,resmin,index,maxmod;
            int a,N,integer;
            Random rand = new Random();
            Console.Write("N = "); N = int.Parse(Console.ReadLine());
            double[] mas = new double[N];
            for (integer=0,b=0,index=0,max=-101,c=0,a = 0,resmin=0,maxmod=-101;a<N;a++)
            {
                mas[a] = (-1000 + rand.Next(2000));
                mas[a]/=10;
                Console.Write("{0}){1}  ", a,mas[a]);
                if (mas[a]<0)
                {
                    resmin = c + mas[a];
                    c=resmin;
                }
                if (mas[a] > max)
                {
                    max = mas[a];
                    index = a + 1;
                }
                if (Math.Abs(mas[a]) > maxmod)
                {
                    maxmod = Math.Abs(mas[a]);
                    b = mas[a];
                }
                if (Math.Truncate(mas[a])==mas[a])
                {
                    integer = 1 + integer;
                }
            }
            Console.WriteLine();
            Console.WriteLine("1. The sum of of negative elements = {0}", resmin);
            Console.WriteLine("2. Max element = {0}",max);
            Console.WriteLine("3. Index of max element = {0}", index);
            Console.WriteLine("4. Max element in module = {0}", b);
            Console.WriteLine("5. Integer = {0}", integer);
            Console.ReadLine();
        }
    }
}
